import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';

@Injectable({
  providedIn: 'root'
})
export class WebsocketService {

  constructor(private socket: Socket) { }

  wsRegisterCompany(company: any) {
    this.socket.emit('wsregcompany', company)
  }

  wsLoadMenu(menu: any) {
    this.socket.emit('wsloamenu', menu)
  }

  wsgetOnTravel(order: any)
  {
    this.socket.emit('msgviaje', order)
  }

  wsgetOnAplly(order: any)
  {
    this.socket.emit('apply', order)
  }

  wsupdateAccount(account: any)
  {
    this.socket.emit('wsupdateaccount', account)
  }

  wsusercompany(account: any)
  {
    this.socket.emit('wsusercompany', account)
  }

  getOnTravel(order: any)
  {
  this.socket.emit('msgviaje', order)
  }

  getAntecedentes(info: any)
  {
  this.socket.emit('docantecedente', info)
  }

}
