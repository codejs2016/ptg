import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataProductsService {

  public storage: any;
  public cart: any;
  public products: any;
  public company: any;
  public city: any;
  public order: any;
  public brachoffice: any;
  public invoice: any;
  public delivery = 3000;

  constructor() { }
}
