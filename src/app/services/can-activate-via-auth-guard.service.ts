import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { CanActivate } from '@angular/router';
import { LoginService } from './login.service';


@Injectable({
  providedIn: 'root'
})
export class CanActivateViaAuthGuardService {

  constructor(private loginServices: LoginService, private router: Router) { }
}
