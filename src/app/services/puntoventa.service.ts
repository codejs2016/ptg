import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from './config-service';


@Injectable({
  providedIn: 'root'
})
export class PuntoventaService {

  readonly URL_API = this.config.getConfig().bussinesServer.url

  constructor(private http: HttpClient, private config:ConfigService)
  {
  }
  
  getPtvs()
  {
    return this.http.get(this.URL_API+"/puntos.php")
  }

  putPtvs(punto: any)
  {
    return this.http.post(this.URL_API + "/updatepunto.php", punto)
  }

}
