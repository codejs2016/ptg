import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { ConfigService } from "./config-service";

@Injectable({
  providedIn: 'root'
})
export class MateriaService {

  readonly URL_API = this.config.getConfig().bussinesServer.url

  constructor(private http: HttpClient, private config:ConfigService) {
  }

  getUnidadMedidas()
  {
    return this.http.get(this.URL_API+'/unidadMedida.php')
  }

  getMateriaPrima()
  {
    return this.http.get(this.URL_API+'/materiaprima.php')
  }

  post(user:any)
  {
    return this.http.post(this.URL_API+'/createmateria.php', user)
  }

  put(user:any)
  {
    return this.http.post(this.URL_API+'/updatemateria.php', user)
  }

  delete(data: any)
  {
    return this.http.post(this.URL_API + "/deletemateria.php",data)
  }

  chargePoint(data:any)
  {
    return this.http.post(this.URL_API+'/chargepoint.php', data)
  }

}
