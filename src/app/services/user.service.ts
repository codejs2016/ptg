import { Injectable, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from './config-service';


@Injectable({
  providedIn: 'root'
})
export class UserService implements OnInit {
  
  readonly URL_API = this.config.getConfig().bussinesServer.url

  constructor(private http: HttpClient, private config:ConfigService) {
  }

  ngOnInit(){}

  getUsers()
  {
    return this.http.get(this.URL_API+'/getuser.php')
  }

  postUser(user:any)
  {
    return this.http.post(this.URL_API+'/createuser.php', user)
  }

  putUser(data: any)
  {
    return this.http.post(this.URL_API+'/updateuser.php',data)
  }

  putPassword(data: any)
  {
    return this.http.post(this.URL_API+'/updatepassword.php',data)
  }

  putPoint(data: any)
  {
    return this.http.post(this.URL_API+'/updatepoint.php',data)
  }

  deleteUser(id: any)
  {
    return this.http.put(this.URL_API+'/deleteuser.php',id)
  }

}
