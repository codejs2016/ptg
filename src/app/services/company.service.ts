import { Injectable, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from './config-service';

@Injectable({
  providedIn: 'root'
})
export class CompanyService implements OnInit {

  readonly URL_API = this.config.getConfig().bussinesServer.url

  constructor(
    private http: HttpClient, 
    private config:ConfigService)
  {}
   
  ngOnInit(){}

  getCompanys()
  {
    return this.http.get(this.URL_API +"/api/company")
  }

  searchsCompany(company: any)
  {
    return this.http.post(this.URL_API +"/api/company/search", company)
  }

  getCompanyByCity(id: string)
  {
    return this.http.get(this.URL_API +"/api/company/city" + `/${id}`)
  }

  getCompanyById(id:any)
  {
    return this.http.get(this.URL_API+"/api/company/"+id)
  }

  getCompanyByUser(id: string)
  {
    return this.http.get(this.URL_API +"/api/company/user" + `/${id}`)
  }

  registerCompany(company: any)
  {
    return this.http.post(this.URL_API +"/api/company/create", company)
  }

  updateCompany(company: any)
  {
    return this.http.put(this.URL_API + `/api/company/${company._id}`, company)
  }

  deleteCompany(_id: string){
    return this.http.delete(this.URL_API + "/api/company"+ `/${_id}`)
  }

}
