import { Injectable, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from './config-service';

@Injectable({
  providedIn: 'root'
})
export class CategoryService implements OnInit{

  readonly URL_API = this.config.getConfig().bussinesServer.url

  constructor(private http: HttpClient, private config:ConfigService)
  {
  }
 
  ngOnInit()
  {
   
  }

  getAllCategory()
  {
    return this.http.get(this.URL_API+"/api/category/")
  }

  getCategorys(name:String)
  {
    return this.http.get(this.URL_API+"/api/category/type"+`/${name}`)
  }

  getCategory(id:any)
  {
    return this.http.get(this.URL_API+"/api/category/"+id)
  }
}
