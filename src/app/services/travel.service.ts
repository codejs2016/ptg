import { Injectable, OnInit } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { ConfigService } from "./config-service";

@Injectable({
  providedIn: "root"
})
export class TravelService implements OnInit {

  readonly URL_API = this.config.getConfig().bussinesServer.url;

  constructor(private http: HttpClient, private config: ConfigService) {}
  
  ngOnInit() {}

  gets() 
  {
    return this.http.get(this.URL_API + "/productos.php")
  }
  
  getsOnlyIdAndName() 
  {
    return this.http.get(this.URL_API + "/productosonly.php")
  }

  register(product: any)
  {
    return this.http.post(this.URL_API + "/createproducto.php", product)
  }
  
  putProduct(product: any)
  {
    return this.http.post(this.URL_API + "/updateproducto.php", product)
  }

  deleteTravel(data: any)
  {
    return this.http.post(this.URL_API + "/deleteproducto.php",data)
  }
}
