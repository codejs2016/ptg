import { Injectable, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from './config-service';

@Injectable({
  providedIn: 'root'
})
export class CityService implements OnInit {

  readonly URL_API = this.config.getConfig().bussinesServer.url

  constructor(
    private http: HttpClient, 
    private config:ConfigService)
  {}
   
  ngOnInit()
  {
  }

  listCarga()
  {
    return this.http.get(this.URL_API+"/api/city/carga")
  }

  listVehiciles()
  {
    return this.http.get(this.URL_API+"/api/city/vehicles")
  }


  listPertos()
  {
    return this.http.get(this.URL_API+"/api/city/puertos")
  }

  listCity()
  {
    return this.http.get(this.URL_API+"/api/city")
  }

  listCompanyByCity()
  {
    return this.http.get(this.URL_API+"/api/city/citycompanys")
  }
  
  searchsCity(city: any)
  {
    return this.http.post(this.URL_API+"/api/city/search", city)
  }

  getCity(id:any)
  {
    return this.http.get(this.URL_API+"/api/city/"+id)
  }

  registerCity(city: any)
  {
    return this.http.post(this.URL_API+"/api/city/create", city)
  }

  updateCity(city: any)
  {
    return this.http.put(this.URL_API+"/api/city/upadate", city)
  }

  deleteCity(id: any)
  {
    return this.http.delete(this.URL_API+"/api/city/", id)
  }
} 
