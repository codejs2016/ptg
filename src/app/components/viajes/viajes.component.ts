import { Component, OnInit } from '@angular/core';
import { FormGroup, AbstractControl, Validators, FormBuilder, FormControl } from '@angular/forms';
import { CityService } from 'src/app/services/city.service';
import { DataService } from 'src/app/services/data.service';
import { TravelService } from 'src/app/services/travel.service';
import { ConfigService } from 'src/app/services/config-service';
import { Router, ActivatedRoute } from '@angular/router';
import swal from 'sweetalert2';

@Component({
  selector: 'app-viajes',
  templateUrl: './viajes.component.html',
  styleUrls: ['./viajes.component.css']
})
export class ViajesComponent implements OnInit {

  public id: any
  public showSpinner = false
  public showListViajes = false
  public showFormViajes = false
  public showgrid = true
  public showform = false
  public submitted = false

  public editedRowIndex: number
  public tipoproducto: any
  public allTravels: any
  public formatOptions: any = {
    style: 'currency',
    currency: 'COP',
    currencyDisplay: 'COP'
  };

  public listTravelByCompany: any

  public fielrequired = "campo requerido"

  public registerForm: FormGroup
  public nombre: AbstractControl
  public precio: AbstractControl
  public tipo: AbstractControl

  constructor(private formBuilder: FormBuilder,
    private cityService: CityService,
    private dattaService: DataService,
    private travelService: TravelService,
    private config: ConfigService,
    private router: Router,
    private route: ActivatedRoute
  ) {

    this.registerForm = this.formBuilder.group({
      _id: '',
      nombre: ['', Validators.required],
      precio: ['', Validators.required],
      tipo: ['', Validators.required]
    })

    this.nombre = this.registerForm.controls['nombre']
    this.precio = this.registerForm.controls['precio']
    this.tipo = this.registerForm.controls['tipo']
  }

  ngOnInit() {
    this.getProductos()
    this.tipoproducto = this.config.getConfig().tipoproducto
  }

  get f() {
    return this.registerForm.controls
  }

  showformData() {
    this.showform = !this.showform
    this.showgrid = !this.showgrid
  }

  validateFormatNumber(event: any) {
    event.target.value = (event.target.value + '').replace(/[^0-9]/g, '')
    this.registerForm.value.precio = this.formatNumber(event.target.value, "$")
  }

  showLoader() {
    this.showSpinner = true
  }

  hideLoader() {
    this.showSpinner = false
  }

  getProductos() {
    this.showLoader()
    this.travelService.gets().subscribe((res: any) => {
      this.allTravels = res.info
      this.hideLoader()
    })
  }

  clear() {
    this.registerForm.reset()
  }

  register() {
    this.submitted = true
    if ((this.registerForm.invalid))
      return

    this.showLoader()
    this.registerForm.value.precio = this.registerForm.value.precio.replace("$", "")
    let preciofinal = this.registerForm.value.precio.replace(",", "")
    this.registerForm.value.precio = preciofinal
    this.showSpinner = true
    this.travelService.register(this.registerForm.value).subscribe((res: any) => {
      if (res.status == true) {
        swal.fire("Registro", "Producto Registrado satisfactoriamente !", "success");
        this.resetForm(this.registerForm)
        this.showformData()
        this.submitted = false
        this.getProductos()
      }
    })
  }


  formatNumber(num: any, prefix: any) {
    prefix = prefix || '';
    num += '';
    var splitStr = num.split('.');
    var splitLeft = splitStr[0];
    var splitRight = splitStr.length > 1 ? '.' + splitStr[1] : '';
    var regx = /(\d+)(\d{3})/;
    while (regx.test(splitLeft)) {
      splitLeft = splitLeft.replace(regx, '$1' + ',' + '$2');
    }
    return prefix + splitLeft + splitRight;
  }

  unformatNumber(num: any) {
    return num.replace(/([^0-9\.\-])/g, '') * 1;
  }
  
  closeEditor(grid: any, rowIndex = this.editedRowIndex) {
    grid.closeRow(rowIndex);
    this.editedRowIndex = undefined;
    this.registerForm = undefined;
  }

  editHandler({ sender, rowIndex, dataItem }) {
    this.closeEditor(sender);
    this.registerForm = new FormGroup({
      '_id': new FormControl(dataItem.idproducto),
      'nombre': new FormControl(dataItem.nombre),
      'precio': new FormControl(dataItem.precio),
      'tipo': new FormControl(dataItem.tipo)
    });
    this.editedRowIndex = rowIndex;
    sender.editRow(rowIndex, this.registerForm);
  }

  cancelHandler({ sender, rowIndex }) {
    this.closeEditor(sender, rowIndex);
  }

  saveHandler({ sender, rowIndex, formGroup, isNew }) {
    this.showLoader()
    this.travelService.putProduct(formGroup.value).subscribe((res: any) => {
      if (res.status == true) {
        swal.fire("Registro", "Producto Actualizado satisfactoriamente !", "success");
        this.getProductos()
      }
    });
    sender.closeRow(rowIndex);
  }

  removeHandler({ dataItem }) {
    this.showLoader()
    const data = {
      idproducto: dataItem.idproducto
    }
    this.travelService.deleteTravel(data).subscribe((res: any) => {
      if (res.status == true) {

        swal.fire("Registro", "Producto Eliminado satisfactoriamente !", "success");
        this.getProductos()
      }
    });
  }

  resetForm(form: any) {
    if (form) {
      form.reset()
    }
  }

  sendMsgTravel(data: any) {
    // this.websocketService.wsgetOnTravel(data)
  }

}
