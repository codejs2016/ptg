import { Component, OnInit } from '@angular/core';
import { PuntoventaService } from 'src/app/services/puntoventa.service';
import { NgbDate, NgbCalendar, NgbDateParserFormatter, NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { TravelService } from 'src/app/services/travel.service';
import { MateriaService } from 'src/app/services/materia.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-secretaria',
  templateUrl: './secretaria.component.html',
  styleUrls: ['./secretaria.component.css']
})
export class SecretariaComponent implements OnInit {

  public allPuntos: any
  public allProducts: any
  public allMateria: any
  public dataSend = []
  public sendPoint = []
  public showSpinner = false
  public showform = true
  public showgrid = false
  private modalRef: NgbModalRef
  public closeResult: string
  public idproducto = null
  public idpunto = null
  public editedRowIndex: number

  constructor(private puntoVentaService: PuntoventaService,
              private travelService: TravelService,
              private materiaService: MateriaService,
              private modalService: NgbModal) { }

  ngOnInit() {
    this.getPtvs()
    this.getProductos()
    this.getMaterias()
  }

  getPtvs(){
    this.showLoader()
    this.puntoVentaService.getPtvs().subscribe( (res:any) =>{
      this.allPuntos = res.info
      this.hideLoader()
    })
  }

  getMaterias(){
    this.showLoader()
    this.materiaService.getMateriaPrima().subscribe( (res:any)=>{
      this.allMateria = res.info
      this.hideLoader()
    })
   }

  getProductos() {
    this.showLoader()
    this.travelService.getsOnlyIdAndName().subscribe((res: any) => {
      this.allProducts = res.info
      this.hideLoader()
    })
  }

  showLoader(){
    this.showSpinner = true
  }

  hideLoader(){
    this.showSpinner = false
  }

  onCloseModal() 
  {
    this.modalRef.close();
  }

  showformData() {
    this.showform = !this.showform
    this.showgrid = !this.showgrid
  }

  getPoint(idpunto:any){
   this.idpunto = idpunto
   this.showformData()
  }

  open(content:any, id:any) {
    this.dataSend = []
    this.idproducto = id
    for (let i = 0; i < this.allProducts.length; i++) {
      if(this.allProducts[i].id ==  id){
        this.allProducts.splice(i,1)
      }
    }
    this.modalService.open(content, { size: 'lg' , scrollable: true })
  }


  closeEditor(grid: any, rowIndex = this.editedRowIndex) {
    grid.closeRow(rowIndex);
    this.editedRowIndex = undefined;
  }
  
  asociar(data:any){
   this.sendPoint.push({ idmateria: data.id, materia: data.materia, cantidad: 0})
   for(let i = 0; i < this.allMateria.length; i++) {
    if(this.allMateria[i].id ==  data.id){
      this.allMateria.splice(i,1)
    }
   }
 }

  delete(id:any) {
    for(let i = 0; i < this.sendPoint.length; i++) {
       if(this.sendPoint[i].idmateria ==  id){
         this.sendPoint.splice(i,1)
       }
    }
  }

  clear(id:any) {
    for(let i = 0; i < this.sendPoint.length; i++) {
       if(this.sendPoint[i].idmateria ==  id){
        this.sendPoint[i].cantidad = ""
       }
    }
  }

  changeCantidad(value:any,id:any){
    for(let i = 0; i < this.sendPoint.length; i++) {
      if(this.sendPoint[i].idmateria ==  id){
       this.sendPoint[i].cantidad = value
      }
    }
  }

  saveInventario(){
    if(this.sendPoint.length > 0){
      let info = {
        idproducto:this.idproducto,
        idpunto:this.idpunto,
        materia:this.sendPoint
      }
      this.materiaService.chargePoint(info).subscribe( (res:any)=>{
       if(res.status == true){
        swal.fire("Registro", "Productod Registradod satisfactoriamente !", "success");
       }else  
         swal.fire("Aviso", "Ocurrio un error al cargar inventario a punto de venta!", "warning");
      })
    }
    else 
      swal.fire("Aviso", "Por favor agregar productos!", "warning");
  }
}
