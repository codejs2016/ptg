import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HistorialvoajesComponent } from './historialvoajes.component';

describe('HistorialvoajesComponent', () => {
  let component: HistorialvoajesComponent;
  let fixture: ComponentFixture<HistorialvoajesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HistorialvoajesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistorialvoajesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
