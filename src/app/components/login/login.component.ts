import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder, AbstractControl } from '@angular/forms';
import { Router } from "@angular/router";

import { DataService } from "src/app/services/data.service";
import { MessageService } from "src/app/services/message.service";
import { ConfigService } from 'src/app/services/config-service';
import { LoginService } from 'src/app/services/login.service';
import { CompanyService } from 'src/app/services/company.service';


import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public dateNow = new Date()
  public lottieConfigSuccess: Object
  public loginComponentText: any;
  public loginForm: FormGroup;
  public user: any;
  public showModal = true
  public showSpinner = false
  public username: AbstractControl
  public password: AbstractControl

  constructor(
    private formBuilder: FormBuilder,
    private config: ConfigService,
    private loginService: LoginService,
    private companyService: CompanyService,
    private data: DataService,
    private router: Router
  )
  {
    this.loginForm = this.formBuilder.group({
      username: ["", Validators.required ],
      password: ["", Validators.required ]
    });
  }

  ngOnInit() {
    this.loginComponentText = this.getTextLogin();
    this.lottieConfigSuccess = 
    {
      path: 'assets/animation/5147-7-day-free-trial-security.json',
      loop: true
    };
  }


  
  login()
  {
   if(this.loginForm.value )
   {
      this.showSpinner = true
      this.loginService.loginUser( this.loginForm.value ).subscribe( (res: any) => {
        if(res.success == true)
        {
          this.router.navigate(["/admin"])
          this.resetForm(this.loginForm);
        }else{
          Swal.fire("Inicio de sesión ",res.message, "warning")
          this.showSpinner = false
        }
  
      })
    }else{
     Swal.fire("Aviso ","Por favor ingresar usuario y contraseña ", "warning")
     this.showSpinner = false
    }
  }


  pushClientPage(res:any) {
    this.data.storage = {
      user: res.user
    };

    switch (res.user.role)
    {
      case this.config.getConfig().typerol.empresatransporte:
        const data =
        {
          idUser:res.token
        }
        localStorage.setItem('user', res.token)
        //this.webSockeService.wsLoadMenu(data)
       break;
    }
  }


  resetForm(form: any) {
    if (form) {
      form.reset();
    }
  }

  getTextLogin() {
    return this.config.getConfig().loginComponent;
  }


}
