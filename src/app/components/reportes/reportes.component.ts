import { Component, OnInit } from '@angular/core';
import { PuntoventaService } from 'src/app/services/puntoventa.service';
import swal from 'sweetalert2';
import { FormGroup, AbstractControl, FormBuilder, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-reportes',
  templateUrl: './reportes.component.html',
  styleUrls: ['./reportes.component.css']
})
export class ReportesComponent implements OnInit {

  public showSpinner = false
  public showListViajes = false
  public showFormViajes = false
  public showgrid = true

  public allTravels: any
  public editedRowIndex: number

  public registerForm: FormGroup
  public nombre: AbstractControl
  public direccion: AbstractControl
  public domicilios: AbstractControl

  constructor(
    private puntoVentaService: PuntoventaService,
    private formBuilder: FormBuilder) { 
      
      this.registerForm = this.formBuilder.group({
        id: '',
        nombre: ['', Validators.required],
        direccion: ['', Validators.required],
        domicilios: ['', Validators.required]
      })
  
      this.nombre = this.registerForm.controls['nombre']
      this.direccion = this.registerForm.controls['direccion']
      this.domicilios = this.registerForm.controls['domicilios']
    }

  ngOnInit() {
    this.getPtvs()
  }

  getPtvs(){
    this.showLoader()
    this.puntoVentaService.getPtvs().subscribe( (res:any) =>{
      this.allTravels = res.info
      this.hideLoader()
    })
  }

  showLoader(){
    this.showSpinner = true
  }

  hideLoader(){
    this.showSpinner = false
  }

  closeEditor(grid: any, rowIndex = this.editedRowIndex) {
    grid.closeRow(rowIndex);
    this.editedRowIndex = undefined;
    this.registerForm = undefined;
  }

  editHandler({ sender, rowIndex, dataItem }) {
    this.closeEditor(sender);
    this.editedRowIndex = rowIndex; 
    this.registerForm = new FormGroup({
      'id': new FormControl(dataItem.id),
      'nombre': new FormControl(dataItem.nombre),
      'direccion': new FormControl(dataItem.direccion),
      'domicilios': new FormControl(dataItem.domicilios)
    });
    
    this.editedRowIndex = rowIndex;
    sender.editRow(rowIndex, this.registerForm);
  }

  cancelHandler({ sender, rowIndex }) {
    this.closeEditor(sender, rowIndex);
  }

  saveHandler({ sender, rowIndex, formGroup, isNew }) {
    this.showLoader()
    this.puntoVentaService.putPtvs(formGroup.value).subscribe((res: any) => {
      if (res.status == true) {
        swal.fire("Registro", "Punto de Venta Actualizado satisfactoriamente !", "success");
        this.getPtvs()
      }
    });
    sender.closeRow(rowIndex);
  }

}
