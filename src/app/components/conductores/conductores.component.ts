import { Component, OnInit } from '@angular/core';
import { MateriaService } from 'src/app/services/materia.service';
import { FormGroup, AbstractControl, FormBuilder, Validators, FormControl } from '@angular/forms';
import swal from 'sweetalert2';
import { ConfigService } from 'src/app/services/config-service';

@Component({
  selector: 'app-conductores',
  templateUrl: './conductores.component.html',
  styleUrls: ['./conductores.component.css']
})
export class ConductoresComponent implements OnInit {

  public id: any
  public showSpinner = false
  public showListViajes = false
  public showFormViajes = false
  public showgrid = true
  public showform = false
  public submitted = false

  public editedRowIndex: number
  public typeMedida: any
  public allTravels: any
  public undMedida: any
  public dateNow = new Date()

  public registerForm: FormGroup
  public materia: AbstractControl
  public medida: AbstractControl

  constructor(private materiaService: MateriaService,
              private formBuilder: FormBuilder,
              private configService: ConfigService) { 

                this.registerForm = this.formBuilder.group({
                  id: '',
                  materia: ['', Validators.required],
                  medida: ['', Validators.required],
                })
            
                this.materia = this.registerForm.controls['materia']
                this.medida = this.registerForm.controls['medida']
              }

  ngOnInit() {
    this.typeMedida = this.getTypeMedida()
    this.getMaterias()
  }

  get f() {
    return this.registerForm.controls
  }

  getTypeMedida(){
   return this.typeMedida = this.configService.getConfig().medidas
  }

  getUnidadMedidas(){
    this.materiaService.getUnidadMedidas().subscribe( (res:any)=>{
      this.undMedida = res.info
      this.getMaterias()
    })
  }

  getMaterias(){
   this.showLoader()
   this.materiaService.getMateriaPrima().subscribe( (res:any)=>{
     this.allTravels = res.info
     this.hideLoader()
   })
  }

  showformData() {
    this.showform = !this.showform
    this.showgrid = !this.showgrid
  }

  showLoader() {
    this.showSpinner = true
  }

  hideLoader() {
    this.showSpinner = false
  }

  clear(){
    this.registerForm.reset()
  }


  register() {
    this.submitted = true
    if ((this.registerForm.invalid))
      return

    this.showLoader()
    this.showSpinner = true
    this.materiaService.post(this.registerForm.value).subscribe((res: any) => {
      if (res.status == true) {
        swal.fire("Registro", "Materia Prima Registrada satisfactoriamente !", "success");
        this.registerForm.reset()
        this.showformData()
        this.submitted = false
        this.getMaterias()
      }
    })
  }

  closeEditor(grid: any, rowIndex = this.editedRowIndex) {
    grid.closeRow(rowIndex);
    this.editedRowIndex = undefined;
    this.registerForm = undefined;
  }

  editHandler({ sender, rowIndex, dataItem }) {
    this.closeEditor(sender);
    this.registerForm = new FormGroup({
      'id': new FormControl(dataItem.id),
      'materia': new FormControl(dataItem.materia),
      'medida': new FormControl(dataItem.medida)
    });
    this.editedRowIndex = rowIndex;
    sender.editRow(rowIndex, this.registerForm);
  }

  cancelHandler({ sender, rowIndex }) {
    this.closeEditor(sender, rowIndex);
  }

  saveHandler({ sender, rowIndex, formGroup, isNew }) {
    this.showLoader()
    this.materiaService.put(formGroup.value).subscribe((res: any) => {
      if (res.status == true) {
        swal.fire("Registro", "Materia Prima Actualizada satisfactoriamente !", "success");
        this.getMaterias()
      }
    });
    sender.closeRow(rowIndex)
  }

  removeHandler({ dataItem }) {
    this.showLoader()
    const data = {
      id: dataItem.id
    }
    this.materiaService.delete(data).subscribe((res: any) => {
      if (res.status == true) {
        swal.fire("Registro", "Materia Prima Eliminada satisfactoriamente !", "success");
        this.getMaterias()
      }
    });
  }
}
