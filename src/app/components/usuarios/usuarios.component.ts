import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { FormGroup, AbstractControl, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ConfigService } from 'src/app/services/config-service';
import swal from 'sweetalert2';
import { PuntoventaService } from 'src/app/services/puntoventa.service';
import { NgbDate, NgbCalendar, NgbDateParserFormatter, NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.css']
})
export class UsuariosComponent implements OnInit {

  public allUser: any
  public rolesUser: any
  public allPtv: any
  public id: any
  public showSpinner = false
  public showgrid = true
  public showform = false
  public submitted = false
  public showPoint = true
  public editedRowIndex: number
  private modalRef: NgbModalRef
  public closeResult: string

  public registerForm: FormGroup
  public chagePassWForm: FormGroup
  public chagePointForm: FormGroup
  public nombre: AbstractControl
  public nombrecompeleto: AbstractControl
  public password: AbstractControl
  public identificacion: AbstractControl
  public rol: AbstractControl
  public telefono: AbstractControl
  public idpunto: AbstractControl
  public newpassword: AbstractControl

  constructor(private userService: UserService,
             private puntoVentaService: PuntoventaService,
             private modalService: NgbModal,
             private config: ConfigService,
             private formBuilder: FormBuilder) { 
    this.registerForm = this.formBuilder.group({
      _id: '',
      nombre: ['', Validators.required],
      nombrecompeleto: ['', Validators.required],
      password: ['', Validators.required],
      identificacion: ['', Validators.required],
      rol: ['', Validators.required],
      telefono: ['', Validators.required],
      idpunto: ['', Validators.required]
    })

    this.nombre = this.registerForm.controls['nombre']
    this.nombrecompeleto = this.registerForm.controls['nombrecompeleto']
    this.password = this.registerForm.controls['password']
    this.identificacion = this.registerForm.controls['identificacion']
    this.rol = this.registerForm.controls['rol']
    this.telefono = this.registerForm.controls['telefono']
    this.idpunto = this.registerForm.controls['idpunto']

    this.chagePassWForm = this.formBuilder.group({
      id:[''],
      newpassword: ['', Validators.required]
    })
    this.newpassword = this.registerForm.controls['newpassword']

    this.chagePointForm = this.formBuilder.group({
      id:[''],
      idpunto: ['', Validators.required]
    })
    this.idpunto = this.chagePointForm.controls['idpunto']
  }

  ngOnInit() {
    this.rolesUser = this.getRoles()
    this.getUsers()
    this.getPtvs()
  }

  get f() {
    return this.registerForm.controls
  }

  get fpw() {
    return this.chagePassWForm.controls
  }

  get fp() {
    return this.chagePointForm.controls
  }

  showPassWord(){
  }

  getPtvs(){
    this.showLoader()
    this.puntoVentaService.getPtvs().subscribe( (res:any) =>{
      this.allPtv = res.info
      this.hideLoader()
    })
  }

  getRoles(){
    return this.rolesUser = this.config.getConfig().typerolapp
  }

  showformData() {
    this.registerForm.reset()
    this.showform = !this.showform
    this.showgrid = !this.showgrid
  }

  showLoader() {
    this.showSpinner = true
  }

  hideLoader() {
    this.showSpinner = false
  }

  validateFormatNumber(event: any) {
    event.target.value = (event.target.value + '').replace(/[^0-9]/g, '')
    this.registerForm.value.identificacion = this.formatNumber(event.target.value, "$")
  }


  formatNumber(num: any, prefix: any) {
    prefix = prefix || '';
    num += '';
    var splitStr = num.split('.');
    var splitLeft = splitStr[0];
    var splitRight = splitStr.length > 1 ? '.' + splitStr[1] : '';
    var regx = /(\d+)(\d{3})/;
    while (regx.test(splitLeft)) {
      splitLeft = splitLeft.replace(regx, '$1' + ',' + '$2');
    }
    return prefix + splitLeft + splitRight;
  }

  clear() {
    this.registerForm.reset()
  }

  closeEditor(grid: any, rowIndex = this.editedRowIndex) {
    grid.closeRow(rowIndex);
    this.editedRowIndex = undefined;
    this.registerForm = undefined;
  }

  open(content:any, id:any) {
    this.id = id
    this.modalService.open(content, { scrollable: true })
  }

  open2(content:any, id:any) {
    this.id = id
    this.modalService.open(content, { scrollable: true })
  }

  onCloseModal() 
  {
    this.modalRef.close();
  }

  updatePass(){
   this.submitted = true
   if(this.chagePassWForm.invalid)
     return
  
   this.chagePassWForm.get('id').setValue(this.id)
   this.userService.putPassword(this.chagePassWForm.value).subscribe( (res:any)=>{
    if(res.status == true){
      swal.fire("Registro", "Contraseña actualizada satisfactoriamente !", "success");
    }
    else 
      swal.fire("Aviso", "Ocurrio un error al actualizar contraseña !", "warning");

    this.onCloseModal()
   })
  }

  changePunto(){
    this.submitted = true
    if(this.chagePointForm.invalid)
      return
   
      console.log( this.chagePointForm.value );
    this.chagePointForm.get('id').setValue(this.id)
    this.userService.putPoint(this.chagePointForm.value).subscribe( (res:any)=>{
     if(res.status == true){
       swal.fire("Registro", "Punto de venta actualizado satisfactoriamente !", "success");
     }
     else 
       swal.fire("Aviso", "Ocurrio un error al cambiar usuario de punto de venta !", "warning");
 
     this.onCloseModal()
    })
  }

  editHandler({ sender, rowIndex, dataItem }) {
    this.closeEditor(sender);
    this.showPoint = false
    this.registerForm = new FormGroup({
      'id': new FormControl(dataItem.id),
      'nombre': new FormControl(dataItem.nombre),
      'identificacion': new FormControl(dataItem.identificacion),
      'nombrecompleto': new FormControl(dataItem.nombrecompleto),
      'password': new FormControl(dataItem.password_usuario),
      'rol': new FormControl(dataItem.rol),
      'estado': new FormControl(dataItem.estado),
      'idpunto': new FormControl(dataItem.idpuntoventa),
      'telefono': new FormControl(dataItem.telefono),
      'punto': new FormControl(dataItem.punto)
    });
    this.editedRowIndex = rowIndex;
    sender.editRow(rowIndex, this.registerForm);
  }

  cancelHandler({ sender, rowIndex }) {
    this.closeEditor(sender, rowIndex);
    this.showPoint = false
  }
  
  getUsers(){
    this.showLoader()
    this.userService.getUsers().subscribe( (res:any) =>{
      for (let i = 0; i < res.info.length; i++) {
       if(res.info[i].idpuntoventa == 1){
          res.info[i].punto = "Gran Colombiana Principal"
        }
        if(res.info[i].idpuntoventa == 2){
          res.info[i].punto = "Malecon"
        }
        if(res.info[i].idpuntoventa == 3){
          res.info[i].punto = "El pailon"
        }
        if(res.info[i].idpuntoventa == 4){
          res.info[i].punto = "Bellavista"
        }
        if(res.info[i].idpuntoventa == 5){
          res.info[i].punto = "La independencia"
        }
        if(res.info[i].idpuntoventa == 11){
          res.info[i].punto = "Gran Colombiana Avenida"
        }
      }
      this.allUser = res.info
    })
  }

  saveHandler({ sender, rowIndex, formGroup, isNew }) {
    this.showLoader()
    this.userService.putUser(formGroup.value).subscribe((res: any) => {
      if (res.status == true) {
        swal.fire("Registro", "Usuario Actualizado satisfactoriamente !", "success");
        this.getUsers()
        this.hideLoader()
      }
      else{
        swal.fire("Aviso", "Ocurrio un error al actualizar usuario!", "warning");
        this.hideLoader()
      }
    });
    
    sender.closeRow(rowIndex);
  }

  removeHandler({ dataItem }) {
    this.showLoader()
    const data = {
      id: dataItem.id
    }
    this.userService.deleteUser(data).subscribe((res: any) => {
      if (res.status == true) {
        swal.fire("Registro", "Usuario Eliminado satisfactoriamente !", "success");
        this.getUsers()
      }
      else
        swal.fire("Aviso", "Ocurrio un error al eliminar usurio !", "warning");
      this.hideLoader()
    });
  }

  save(){
    this.submitted = true
    if(this.registerForm.invalid)
      return false

    this.showLoader()
    this.userService.postUser(this.registerForm.value).subscribe( (res:any)=>{
      if(res.status == true){
        swal.fire("Registro", "Usuario Registrado satisfactoriamente !", "success");
        this.getUsers()
        this.showformData()
        this.hideLoader()
      }
      else{
        swal.fire("Aviso", "Ocurrio Un Error al Registrar Usuario !", "warning"); 
        this.hideLoader()
      }
    })
  }
}
