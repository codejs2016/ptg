import { Component, OnInit } from '@angular/core';
import { ConfigService } from 'src/app/services/config-service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  
  public listModules: any
  public id : any

  constructor(private config: ConfigService,
              private router: Router)
  {}
  
  ngOnInit(){
    
    //console.log( typeof( localStorage.getItem('_id')  ) ) 
    this.id = localStorage.getItem('_id') 
    this.getListModules()
  }
  
  getListModules()
  {
    this.listModules = this.config.getConfig().modulos
  }

  close()
  {
    this.router.navigate(["/login"])
  }
}
