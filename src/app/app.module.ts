import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { LottieAnimationViewModule } from 'ng-lottie';
import { LoginService } from './services/login.service';
import { WebsocketService } from './services/websocket.service';
import { AppRoutingModule } from './app-routing/app-routing.module';
import { RouterModule } from '@angular/router';
import { AdminComponent } from './components/admin/admin.component';
import { NotfoundComponent } from './components/notfound/notfound.component';
import { ViajesComponent } from './components/viajes/viajes.component';
import { ConductoresComponent } from './components/conductores/conductores.component';
import { HistorialvoajesComponent } from './components/historialvoajes/historialvoajes.component';
import { ReportesComponent } from './components/reportes/reportes.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { ButtonsModule } from '@progress/kendo-angular-buttons';
import '@progress/kendo-angular-intl/locales/es/all';
import { GridModule } from '@progress/kendo-angular-grid';
import { SecretariaComponent } from './components/secretaria/secretaria.component';
import '@progress/kendo-angular-intl/locales/es/all';
import '@progress/kendo-angular-intl/locales/en/all';
import { DropDownsModule } from '@progress/kendo-angular-dropdowns';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import { CompanyService } from './services/company.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { UsuariosComponent } from './components/usuarios/usuarios.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    AdminComponent,
    NotfoundComponent,
    ViajesComponent,
    ConductoresComponent,
    HistorialvoajesComponent,
    ReportesComponent,
    SecretariaComponent,
    UsuariosComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule,
    LottieAnimationViewModule.forRoot(),
    GridModule,
    ButtonsModule,
    BrowserAnimationsModule,
    DropDownsModule,
    NgbModule,
    SweetAlert2Module.forRoot({
      buttonsStyling: false,
      customClass: 'modal-content',
      confirmButtonClass: 'btn btn-primary',
      cancelButtonClass: 'btn'
  })
  ],
  providers: [
    LoginService,
    CompanyService,
    WebsocketService,
    { provide: LOCALE_ID, useValue: 'es' }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
