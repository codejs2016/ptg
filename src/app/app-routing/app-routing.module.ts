import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from '../components/login/login.component';
import { AdminComponent } from '../components/admin/admin.component';
import { ViajesComponent } from '../components/viajes/viajes.component';
import { HistorialvoajesComponent } from '../components/historialvoajes/historialvoajes.component';
import { ConductoresComponent } from '../components/conductores/conductores.component';
import { NotfoundComponent } from '../components/notfound/notfound.component';
import { ReportesComponent } from '../components/reportes/reportes.component';
import { SecretariaComponent } from '../components/secretaria/secretaria.component';
import { UsuariosComponent } from '../components/usuarios/usuarios.component';

const routes: Routes = [
  { 
    path: 'admin', component: AdminComponent,
    children: [
      { path: 'productos', component: ViajesComponent },
      { path: 'puntos', component: ReportesComponent },
      { path: 'materia', component: ConductoresComponent },
      { path: 'carga', component: SecretariaComponent },
      { path: 'usuarios', component: UsuariosComponent }
    ],
  //  canActivate: [ LoginGuard ]
  },
  { path: 'login', component: LoginComponent },
  {
   path: 'not-found',
    component: NotfoundComponent
  },
  {
    path: '**',
    redirectTo: 'login'
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    CommonModule
  ],
  exports:[
    RouterModule,
  ],
  declarations: []
})
export class AppRoutingModule { }
