export const configuration = {
   bussinesServer:{
    url:'http://localhost/api',
   },
   initComponent:{
    login:'Entrar',
    register:'Registrar',
    quicksales:'Comidas Rapidas PuntoG',
    },
    loginComponent:{
     titlte:'Comidas Rapidas PuntoG',
     placeholderUser:'Usuario',
     placeholderPassword:'Contraseña',
     textLogin:'Iniciar sesión' 
    },
    resetComponent:{
      titlte:'Comidas Rapidas PuntoG',
      placeholderUser:'Usuario',
      textreset:'Recuperar contraseña'
     },
     todo:{
      name:'Todo'
     },
    registerComponent:{
      placeholderDocument:'Número de documento',
      placeholderType:'Tipo',
      placeholderName:'Nombres',
      placeholderSurName:'Apellidos',
      placeholderGender:'Genero',
      placeholderAdress:'Dirección',
      placeholderPhone:'Celular',
      placeholderEmail:'Correo',
      placeholderUser:'Usuario',
      placeholderPassword:'Contraseña',
      placeholderRepeatPassword:'Repetir Contraseña',
      accept:'Aceptar',
      yes:'Si',
      no:'No'
    },
    modulos:[
      { name:'Productos', icon:'ion-ios-pizza', call:'productos' }, 
      { name:'Puntos de ventas', icon:'ion-ios-business', call:'puntos' }, 
      { name:'Materia Prima', icon:'ion-ios-apps', call:'materia' },
      { name:'Cargar a PTV', icon:'ion-ios-stats', call:'carga' },
      { name:'Usuarios', icon:'ion-ios-person', call:'usuarios' },
      { name:'Cerrar Sesión', icon:'ion-ios-close', call:'close' }
    ],
    tipoproducto:[
      { name:'Comidas Rapidas', value:'comida rapida' }, 
      { name:'Parrilla', value:'parrilla'}, 
    ],
    typedocuments:{
      documents:[
        { id:'5ca57bb35e9dc70d6ac5a5c9', type: 'Cedula' },
        { id:'5ca57bb35e9dc70d6ac5a5ca', type: 'Cedula extranjera' },
        { id:'5ca57bb35e9dc70d6ac5a5cb', type: 'Pasaporte' }
      ]
    },
    typegender:{
      female:'5c342fd24f5fbf23e32dc8e1',
      male:'5c342fd24f5fbf23e32dc8e2'
    },
    typerol:{
      admin:'5ca579e80f91700d0b03cd04',
      conductor:'5ca579e80f91700d0b03cd05',
      empresatransporte:'5ca579e80f91700d0b03cd06'
    },
    medidas:[
      { id: 1, name:'GR' },
      { id: 2, name:'UNIDAD'},
      { id: 3, name:'RJ'},
      { id: 4, name:'TJ'},
      { id: 5, name:'RD'},
      { id: 6, name:'TIRA'},
      { id: 7, name:'PC'},
      { id: 8, name:'GRAMO'}
    ],
    typerolapp:[
      { name: 'supervisor' },
      { name: 'cajero' }
    ]
}