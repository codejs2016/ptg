<?php
  //error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);
	class clase
	{
		private $host;
		private $bd;
		private $user;
		private $pass;
		private $link;

		function clase($host,$bd,$user,$pass)
		{
			$this->host=$host;
			$this->bd=$bd;
			$this->user=$user;
			$this->pass=$pass;
    	}
		function conectar()
		{
			if(!($this->link=mysqli_connect($this->host,$this->user,$this->pass)))
			{
			    echo"no se pudo conectar al servidor";
			}
			else
			{
				if(!(mysqli_select_db($this->link,$this->bd)))
				{
					echo "no se pudo seleccionar la base de datos";
				}
				else
				{
					return $this->link;
				}
			}
		}
		function query($sql)
		{
			$this->conectar();
            $this->result=mysqli_query($this->link,$sql);
            mysqli_close($this->link);
			return $this->result;
		}
        
        function numrows( $result )
        {
             return mysqli_num_rows( $result );
        }
        function fetch($result)
        {
            return mysqli_fetch_row( $result );
        }
        function assoc($result)
        {
            return mysqli_fetch_assoc( $result );
        }
        function fetchall($result)
        {
            return mysqli_fetch_all( $result );
        }

        function selectOption($config, $label)
        {
    		  
    			$this->conectar();
                
                $consulta= $this->query($config['sql']);
                if( $config['multiple'] == true )
                {
                	$config['multiple'] = "multiple=multiple";
                }
                if( $config['readonly'] == true )
                {
                	$config['readonly'] = "readonly=readonly";
                }
                if( $config['disabled'] == true )
                {
                	$config['disabled'] = "disabled=disabled";
                }
                $class = $config['class'];
                $id = $config['id'];
                $name = $config['name'];
                $name = $config['name'];
                $seleccione = $config['seleccione'];
                $disabled = $config['disabled'];
                $readonly = $config['readonly'];
                $multiple = $config['multiple'];
                $todos = $config['todos'];
                $value = $config['value'];

                $select="<select name='$name' id='$id'  class='$class' $disabled $multiple $readonly   >";

               
                if(mysqli_num_rows($consulta)>0)
                {                     
                    if( $seleccione == true )
                      $select.="<option value='' >SELECCIONE $label </option>";

                    if( $todos == true )
                      $select.="<option value='' >TODOS</option>";
                    
               
                    while( ($fila = mysqli_fetch_row($consulta) ) )
        			{
                         $fila[1] = strtoupper($fila[1]);
                        if( $value == $fila[0])
                        {
        				    $select.="<option value='$fila[0]' selected >$fila[1]</option>";                       
                        }
                        else
                        $select.="<option value='$fila[0]'>$fila[1]</option>";
        			}
                }
                
                $select.="</select>";
                
    			return $select;
    	}
    }
?>
