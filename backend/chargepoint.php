<?php

header('Access-Control-Allow-Origin: *');
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Allow: GET, POST, OPTIONS, PUT, DELETE");
$method = $_SERVER['REQUEST_METHOD'];
if($method == "OPTIONS") 
{
  die();
}
$_POST = json_decode(file_get_contents('php://input'), true);
require 'conexion/conexion.php';
$mysql = new clase("201.148.107.133","puntogco_ventas","puntogco_root","developer88*.");
$mysql->conectar();

$fecha= date('Y-m-d'); 
$hora = date("H:i:s");
$var_ProductPoint = array();
$var_idmateria = array();
$var_materia = array();
$var_cantidad= array();
if( !empty($_POST['idproducto']) && !empty($_POST['idpunto'])  &&  !empty($_POST['materia']) )
{
    $idproducto = $_POST['idproducto'];
    $idpunto = $_POST['idpunto'];
    foreach($_POST["materia"] as $obs)
    {
        $var_idmateria = $obs['idmateria']; 
        $var_cantidad = $obs['cantidad']; 
        $var_materia = $obs['materia']; 
        $sql = "INSERT INTO materiaprimapproducto VALUES 
        (NULL,$var_idmateria,$idproducto,$var_cantidad,$var_cantidad,0,0,0,0,0,$idpunto,'$fecha','$hora',0)";
        $mysql->query($sql);
    }
    echo json_encode(array( 'status' => true ) );
}
else
  echo json_encode(array( 'status' => false ) );
?>